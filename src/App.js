import Navbar from "./components/Navbar";
import Routes from "./routes";

const App = () => {
  return (
    <>
      <Navbar />
      <div className="container">
        <Routes />
      </div>
    </>
  );
};

export default App;
