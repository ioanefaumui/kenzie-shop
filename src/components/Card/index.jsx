import "./style.css";
import { useDispatch } from "react-redux";
import {
  addProductThunk,
  removeProductThunk,
} from "../../store/modules/cart/thunks";

const Card = ({ product, isRemovable = false }) => {
  const { id, name, image, price } = product;

  const dispatch = useDispatch();

  return (
    <div>
      <div className="image" style={{ backgroundImage: `url(${image})` }}></div>
      <h4>{name}</h4>
      <span>R$ {price}</span>
      <div>
        {isRemovable ? (
          <button
            className="cart-button"
            onClick={() => dispatch(removeProductThunk(id))}
          >
            REMOVE PRODUCT
          </button>
        ) : (
          <button
            className="cart-button"
            style={{ backgroundColor: "green" }}
            onClick={() => dispatch(addProductThunk(product))}
          >
            ADD PRODUCT
          </button>
        )}
      </div>
    </div>
  );
};

export default Card;
