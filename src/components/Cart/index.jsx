import { useSelector } from "react-redux";
import "./style.css";
import { motion } from "framer-motion";
import { useDispatch } from "react-redux";
import { removeProductThunk } from "../../store/modules/cart/thunks";
import { Link } from "react-router-dom";

const variants = {
  visible: (i) => ({
    opacity: 1,
    transition: {
      delay: i * 0.1,
    },
    y: 0,
  }),
  hidden: { opacity: 0, y: 50 },
};

const Cart = () => {
  const cart = useSelector((store) => store.cart);

  const dispatch = useDispatch();

  console.log(cart);
  return (
    <>
      <h2>My products</h2>

      {cart &&
        cart.map((product, i) => (
          <motion.div
            className="info"
            key={product.id}
            initial="hidden"
            custom={i}
            animate="visible"
            variants={variants}
          >
            <div className="left">
              <div
                style={{ backgroundImage: `url(${product.image})` }}
                className="cart-image"
              ></div>
              <span>1 x {product.name}</span>
              <button
                onClick={() => dispatch(removeProductThunk(product.id))}
                className="remove-button"
              >
                REMOVE
              </button>
            </div>
            <div>
              <span>R$ {product.price}</span>
            </div>
          </motion.div>
        ))}

      {cart.length > 0 && (
        <div className="total">
          <div style={{ display: "flex", justifyContent: "space-between" }}>
            <span>Total: </span>
            <span>
              R$ {cart.reduce((acc, product) => acc + product.price, 0)}
            </span>
          </div>
          <div>
            <button className="cart-button" style={{ background: "green" }}>
              PROCEED TO CHECKOUT
            </button>
          </div>
        </div>
      )}

      {cart.length === 0 && (
        <div className="empty-cart">
          <h3>No products added</h3>
          <Link to="/">Back to products</Link>
        </div>
      )}
    </>
  );
};

export default Cart;
