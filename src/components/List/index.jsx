import Card from "../Card";
import "./style.css";
import { useSelector } from "react-redux";
import { motion } from "framer-motion";

const variants = {
  visible: (i) => ({
    opacity: 1,
    transition: {
      delay: i * 0.1,
    },
    y: 0,
  }),
  hidden: { opacity: 0, y: 50 },
};

const List = () => {
  const products = useSelector((store) => store.products);
  return (
    <>
      <h2>Popular frames</h2>
      <motion.ul className="list">
        {products.map((product, i) => (
          <motion.li
            className="card"
            key={product.id}
            initial="hidden"
            custom={i}
            animate="visible"
            variants={variants}
          >
            <Card key={product.id} product={product} />
          </motion.li>
        ))}
      </motion.ul>
    </>
  );
};

export default List;
