import { useSelector } from "react-redux";
import logotype from "../../assets/logotype.svg";
import "./style.css";
import { Link } from "react-router-dom";

const Navbar = () => {
  const cart = useSelector((store) => store.cart);

  return (
    <header>
      <div className="container">
        <nav>
          <Link to="/">
            <figure>
              <img src={logotype} alt="kashop brand" />
            </figure>
          </Link>

          <ul className="menu-links">
            <Link to="/my_products" className="menu-item">
              My Products
              <span className={cart.length > 0 ? "cart-filled" : "cart-icon"}>
                {cart.length}
              </span>
            </Link>

            <Link>Logout</Link>
          </ul>
        </nav>
      </div>
    </header>
  );
};

export default Navbar;
