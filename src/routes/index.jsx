import { Route, Switch } from "react-router-dom";
import HomePage from "../pages/HomePage";
import MyProducts from "../pages/MyProducts";

const Routes = () => {
  return (
    <Switch>
      <Route exact path="/">
        <HomePage />
      </Route>
      <Route exact path="/my_products">
        <MyProducts />
      </Route>
    </Switch>
  );
};

export default Routes;
