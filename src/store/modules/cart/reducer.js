import { ADD_PRODUCT, REMOVE_PRODUCT } from "./actionTypes";

const getStore = JSON.parse(localStorage.getItem("cart")) || [];

console.log(getStore);

const cartReducer = (state = getStore, action) => {
  switch (action.type) {
    case ADD_PRODUCT:
      const { product } = action;

      return [...state, product];

    case REMOVE_PRODUCT:
      const { list } = action;

      return list;

    default:
      return state;
  }
};

export default cartReducer;
