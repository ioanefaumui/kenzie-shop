import { addProduct, removeProduct } from "./actions";

export const addProductThunk = (product) => {
  return (dispatch) => {
    const newId =
      Math.floor(Math.random() * 10000000 + 100000) +
      "SHA256" +
      Math.floor(Math.random() * 10000000 + 100000);

    const newProduct = { ...product, id: newId };

    const list = JSON.parse(localStorage.getItem("cart")) || [];

    list.push(newProduct);

    localStorage.setItem("cart", JSON.stringify(list));

    dispatch(addProduct(newProduct));
  };
};

export const removeProductThunk = (id) => (dispatch, getStore) => {
  const { cart } = getStore();
  const list = cart.filter((product) => product.id !== id);
  localStorage.setItem("cart", JSON.stringify(list));
  dispatch(removeProduct(list));
};
