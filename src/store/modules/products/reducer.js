const products = [
  {
    id: 1,
    name: "Pearl Bush",
    price: 199.0,
    image: "https://i.ibb.co/4TCswFj/r1.webp",
    color_group: "EBE1DA2D2A28B16847BD7B58",
  },
  {
    id: 2,
    name: "Dune",
    price: 230.0,
    image: "https://i.ibb.co/wKBcTNB/r2.webp",
    color_group: "EBE1DA2D2A28B16847BD7B58",
  },
  {
    id: 3,
    name: "Cape Palliser",
    price: 199.0,
    image: "https://i.ibb.co/8gvqPmJ/r3.webp",
    color_group: "EBE1DA2D2A28B16847BD7B58",
  },
  {
    id: 4,
    name: "Raw Siena",
    price: 379.0,
    image: "https://i.ibb.co/4SD89Sh/r4.webp",
    color_group: "EBE1DA2D2A28B16847BD7B58",
  },
];

const productsReducer = (state = products) => {
  return state;
};

export default productsReducer;
